<?php

namespace SelworkBundle\Listener;

use SelworkBundle\Factory\RequestFactory;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class RequestListener
{
    /**
     * @var AuthorizationCheckerInterface
     */
    private $authorizationChecker;

    /**
     * @var RequestFactory
     */
    private $requestFactory;

    /**
     * RequestListener constructor.
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param RequestFactory $requestFactory
     */
    public function __construct(AuthorizationCheckerInterface $authorizationChecker, RequestFactory $requestFactory)
    {
        $this->authorizationChecker = $authorizationChecker;
        $this->requestFactory = $requestFactory;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        if ($event->isMasterRequest()) {
            if ($this->authorizationChecker->isGranted('IS_AUTHENTICATED_FULLY')) {
                $request = $this->requestFactory->build($event->getRequest());

                $request->save();
            }
        }
    }
}
