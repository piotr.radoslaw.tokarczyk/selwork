<?php

namespace SelworkBundle\Mail;

use SelworkBundle\DataProvider\StatisticsDataProvider;

class StatisticsMailer
{
    /**
     * @var \Swift_Mailer
     */
    protected $mailer;

    /**
     * @var StatisticsDataProvider
     */
    protected $dataProvider;

    /**
     * @param \Swift_Mailer $mailer
     * @param StatisticsDataProvider $dataProvider
     */
    public function __construct(\Swift_Mailer $mailer, StatisticsDataProvider $dataProvider)
    {
        $this->mailer = $mailer;
        $this->dataProvider = $dataProvider;
    }

    /**
     * @param \DateTime $date
     *
     * @return int
     */
    public function sendForDate(\DateTime $date)
    {
        $count = $this->dataProvider->getRequestCounterByDate($date);
        $now = new \DateTime();

        $body = sprintf(
            "There was: %d requests from %s to %s.",
            $count,
            $date->format('Y-m-d H:i:s'),
            $now->format('Y-m-d H:i:s')
        );

        $subject = sprintf(
            'Request statistics from %s to %s',
            $date->format('Y-m-d H:i:s'),
            $now->format('Y-m-d H:i:s')
        );

        $message = $this->mailer->createMessage()
            ->setSubject($subject)
            ->setFrom('noreply@sel-work.pl')
            ->setTo('piotr.radoslaw.tokarczyk@gmail.com')
            ->setBody($body);

        return $this->mailer->send($message);
    }
}
