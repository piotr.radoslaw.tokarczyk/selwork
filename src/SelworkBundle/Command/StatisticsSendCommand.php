<?php

namespace SelworkBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * This command need to be run hourly by cron
 *
 * @package SelworkBundle\Command
 */
class StatisticsSendCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('selwork:statistics:send')
            ->setDescription('Send statistics for last hour.')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return null|int null or 0 if everything went fine, or an error code
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $date = new \DateTime();
        $date->modify('-1 hour');

        $this->getContainer()->get('selwork.statistics_mailer')->sendForDate($date);
    }
}
