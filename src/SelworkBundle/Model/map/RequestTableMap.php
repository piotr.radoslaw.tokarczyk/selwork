<?php

namespace SelworkBundle\Model\map;

use \RelationMap;
use \TableMap;

/**
 * This class defines the structure of the 'request' table.
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.src.SelworkBundle.Model.map
 */
class RequestTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.SelworkBundle.Model.map.RequestTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('request');
        $this->setPhpName('Request');
        $this->setClassname('SelworkBundle\\Model\\Request');
        $this->setPackage('src.SelworkBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('session_id', 'SessionId', 'VARCHAR', false, 255, null);
        $this->addColumn('ip_address', 'IpAddress', 'VARCHAR', false, 255, null);
        $this->addColumn('request_url', 'RequestUrl', 'VARCHAR', false, 255, null);
        $this->addColumn('post_parameters', 'PostParameters', 'VARCHAR', false, 255, null);
        $this->addColumn('get_parameters', 'GetParameters', 'VARCHAR', false, 255, null);
        $this->addColumn('method', 'Method', 'VARCHAR', false, 255, null);
        $this->addColumn('original_file_name', 'OriginalFileName', 'VARCHAR', false, 255, null);
        $this->addColumn('temporary_file_name', 'TemporaryFileName', 'VARCHAR', false, 255, null);
        $this->addColumn('referrer', 'Referrer', 'VARCHAR', false, 255, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()
} // RequestTableMap
