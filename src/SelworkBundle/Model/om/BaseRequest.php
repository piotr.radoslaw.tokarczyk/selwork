<?php

namespace SelworkBundle\Model\om;

use \BaseObject;
use \BasePeer;
use \Criteria;
use \DateTime;
use \Exception;
use \PDO;
use \Persistent;
use \Propel;
use \PropelDateTime;
use \PropelException;
use \PropelPDO;
use SelworkBundle\Model\Request;
use SelworkBundle\Model\RequestPeer;
use SelworkBundle\Model\RequestQuery;

abstract class BaseRequest extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'SelworkBundle\\Model\\RequestPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        RequestPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinit loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the session_id field.
     * @var        string
     */
    protected $session_id;

    /**
     * The value for the ip_address field.
     * @var        string
     */
    protected $ip_address;

    /**
     * The value for the request_url field.
     * @var        string
     */
    protected $request_url;

    /**
     * The value for the post_parameters field.
     * @var        string
     */
    protected $post_parameters;

    /**
     * The value for the get_parameters field.
     * @var        string
     */
    protected $get_parameters;

    /**
     * The value for the method field.
     * @var        string
     */
    protected $method;

    /**
     * The value for the original_file_name field.
     * @var        string
     */
    protected $original_file_name;

    /**
     * The value for the temporary_file_name field.
     * @var        string
     */
    protected $temporary_file_name;

    /**
     * The value for the referrer field.
     * @var        string
     */
    protected $referrer;

    /**
     * The value for the created_at field.
     * @var        string
     */
    protected $created_at;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [session_id] column value.
     *
     * @return string
     */
    public function getSessionId()
    {
        return $this->session_id;
    }

    /**
     * Get the [ip_address] column value.
     *
     * @return string
     */
    public function getIpAddress()
    {
        return $this->ip_address;
    }

    /**
     * Get the [request_url] column value.
     *
     * @return string
     */
    public function getRequestUrl()
    {
        return $this->request_url;
    }

    /**
     * Get the [post_parameters] column value.
     *
     * @return string
     */
    public function getPostParameters()
    {
        return $this->post_parameters;
    }

    /**
     * Get the [get_parameters] column value.
     *
     * @return string
     */
    public function getGetParameters()
    {
        return $this->get_parameters;
    }

    /**
     * Get the [method] column value.
     *
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * Get the [original_file_name] column value.
     *
     * @return string
     */
    public function getOriginalFileName()
    {
        return $this->original_file_name;
    }

    /**
     * Get the [temporary_file_name] column value.
     *
     * @return string
     */
    public function getTemporaryFileName()
    {
        return $this->temporary_file_name;
    }

    /**
     * Get the [referrer] column value.
     *
     * @return string
     */
    public function getReferrer()
    {
        return $this->referrer;
    }

    /**
     * Get the [optionally formatted] temporal [created_at] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = null)
    {
        if ($this->created_at === null) {
            return null;
        }

        if ($this->created_at === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->created_at);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->created_at, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return Request The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = RequestPeer::ID;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [session_id] column.
     *
     * @param string $v new value
     * @return Request The current object (for fluent API support)
     */
    public function setSessionId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->session_id !== $v) {
            $this->session_id = $v;
            $this->modifiedColumns[] = RequestPeer::SESSION_ID;
        }

        return $this;
    } // setSessionId()

    /**
     * Set the value of [ip_address] column.
     *
     * @param string $v new value
     * @return Request The current object (for fluent API support)
     */
    public function setIpAddress($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->ip_address !== $v) {
            $this->ip_address = $v;
            $this->modifiedColumns[] = RequestPeer::IP_ADDRESS;
        }

        return $this;
    } // setIpAddress()

    /**
     * Set the value of [request_url] column.
     *
     * @param string $v new value
     * @return Request The current object (for fluent API support)
     */
    public function setRequestUrl($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->request_url !== $v) {
            $this->request_url = $v;
            $this->modifiedColumns[] = RequestPeer::REQUEST_URL;
        }

        return $this;
    } // setRequestUrl()

    /**
     * Set the value of [post_parameters] column.
     *
     * @param string $v new value
     * @return Request The current object (for fluent API support)
     */
    public function setPostParameters($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->post_parameters !== $v) {
            $this->post_parameters = $v;
            $this->modifiedColumns[] = RequestPeer::POST_PARAMETERS;
        }

        return $this;
    } // setPostParameters()

    /**
     * Set the value of [get_parameters] column.
     *
     * @param string $v new value
     * @return Request The current object (for fluent API support)
     */
    public function setGetParameters($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->get_parameters !== $v) {
            $this->get_parameters = $v;
            $this->modifiedColumns[] = RequestPeer::GET_PARAMETERS;
        }

        return $this;
    } // setGetParameters()

    /**
     * Set the value of [method] column.
     *
     * @param string $v new value
     * @return Request The current object (for fluent API support)
     */
    public function setMethod($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->method !== $v) {
            $this->method = $v;
            $this->modifiedColumns[] = RequestPeer::METHOD;
        }

        return $this;
    } // setMethod()

    /**
     * Set the value of [original_file_name] column.
     *
     * @param string $v new value
     * @return Request The current object (for fluent API support)
     */
    public function setOriginalFileName($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->original_file_name !== $v) {
            $this->original_file_name = $v;
            $this->modifiedColumns[] = RequestPeer::ORIGINAL_FILE_NAME;
        }

        return $this;
    } // setOriginalFileName()

    /**
     * Set the value of [temporary_file_name] column.
     *
     * @param string $v new value
     * @return Request The current object (for fluent API support)
     */
    public function setTemporaryFileName($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->temporary_file_name !== $v) {
            $this->temporary_file_name = $v;
            $this->modifiedColumns[] = RequestPeer::TEMPORARY_FILE_NAME;
        }

        return $this;
    } // setTemporaryFileName()

    /**
     * Set the value of [referrer] column.
     *
     * @param string $v new value
     * @return Request The current object (for fluent API support)
     */
    public function setReferrer($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->referrer !== $v) {
            $this->referrer = $v;
            $this->modifiedColumns[] = RequestPeer::REFERRER;
        }

        return $this;
    } // setReferrer()

    /**
     * Sets the value of [created_at] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Request The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created_at !== null || $dt !== null) {
            $currentDateAsString = ($this->created_at !== null && $tmpDt = new DateTime($this->created_at)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->created_at = $newDateAsString;
                $this->modifiedColumns[] = RequestPeer::CREATED_AT;
            }
        } // if either are not null

        return $this;
    } // setCreatedAt()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {
            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->session_id = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->ip_address = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->request_url = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->post_parameters = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->get_parameters = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->method = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->original_file_name = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->temporary_file_name = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->referrer = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->created_at = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);
            return $startcol + 11; // 11 = RequestPeer::NUM_HYDRATE_COLUMNS.
        } catch (Exception $e) {
            throw new PropelException("Error populating Request object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(RequestPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = RequestPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(RequestPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = RequestQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(RequestPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                RequestPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            $this->alreadyInSave = false;
        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = RequestPeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . RequestPeer::ID . ')');
        }

        // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(RequestPeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`id`';
        }
        if ($this->isColumnModified(RequestPeer::SESSION_ID)) {
            $modifiedColumns[':p' . $index++]  = '`session_id`';
        }
        if ($this->isColumnModified(RequestPeer::IP_ADDRESS)) {
            $modifiedColumns[':p' . $index++]  = '`ip_address`';
        }
        if ($this->isColumnModified(RequestPeer::REQUEST_URL)) {
            $modifiedColumns[':p' . $index++]  = '`request_url`';
        }
        if ($this->isColumnModified(RequestPeer::POST_PARAMETERS)) {
            $modifiedColumns[':p' . $index++]  = '`post_parameters`';
        }
        if ($this->isColumnModified(RequestPeer::GET_PARAMETERS)) {
            $modifiedColumns[':p' . $index++]  = '`get_parameters`';
        }
        if ($this->isColumnModified(RequestPeer::METHOD)) {
            $modifiedColumns[':p' . $index++]  = '`method`';
        }
        if ($this->isColumnModified(RequestPeer::ORIGINAL_FILE_NAME)) {
            $modifiedColumns[':p' . $index++]  = '`original_file_name`';
        }
        if ($this->isColumnModified(RequestPeer::TEMPORARY_FILE_NAME)) {
            $modifiedColumns[':p' . $index++]  = '`temporary_file_name`';
        }
        if ($this->isColumnModified(RequestPeer::REFERRER)) {
            $modifiedColumns[':p' . $index++]  = '`referrer`';
        }
        if ($this->isColumnModified(RequestPeer::CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = '`created_at`';
        }

        $sql = sprintf(
            'INSERT INTO `request` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id`':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`session_id`':
                        $stmt->bindValue($identifier, $this->session_id, PDO::PARAM_STR);
                        break;
                    case '`ip_address`':
                        $stmt->bindValue($identifier, $this->ip_address, PDO::PARAM_STR);
                        break;
                    case '`request_url`':
                        $stmt->bindValue($identifier, $this->request_url, PDO::PARAM_STR);
                        break;
                    case '`post_parameters`':
                        $stmt->bindValue($identifier, $this->post_parameters, PDO::PARAM_STR);
                        break;
                    case '`get_parameters`':
                        $stmt->bindValue($identifier, $this->get_parameters, PDO::PARAM_STR);
                        break;
                    case '`method`':
                        $stmt->bindValue($identifier, $this->method, PDO::PARAM_STR);
                        break;
                    case '`original_file_name`':
                        $stmt->bindValue($identifier, $this->original_file_name, PDO::PARAM_STR);
                        break;
                    case '`temporary_file_name`':
                        $stmt->bindValue($identifier, $this->temporary_file_name, PDO::PARAM_STR);
                        break;
                    case '`referrer`':
                        $stmt->bindValue($identifier, $this->referrer, PDO::PARAM_STR);
                        break;
                    case '`created_at`':
                        $stmt->bindValue($identifier, $this->created_at, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggreagated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            if (($retval = RequestPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }



            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = RequestPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getSessionId();
                break;
            case 2:
                return $this->getIpAddress();
                break;
            case 3:
                return $this->getRequestUrl();
                break;
            case 4:
                return $this->getPostParameters();
                break;
            case 5:
                return $this->getGetParameters();
                break;
            case 6:
                return $this->getMethod();
                break;
            case 7:
                return $this->getOriginalFileName();
                break;
            case 8:
                return $this->getTemporaryFileName();
                break;
            case 9:
                return $this->getReferrer();
                break;
            case 10:
                return $this->getCreatedAt();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array())
    {
        if (isset($alreadyDumpedObjects['Request'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Request'][$this->getPrimaryKey()] = true;
        $keys = RequestPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getSessionId(),
            $keys[2] => $this->getIpAddress(),
            $keys[3] => $this->getRequestUrl(),
            $keys[4] => $this->getPostParameters(),
            $keys[5] => $this->getGetParameters(),
            $keys[6] => $this->getMethod(),
            $keys[7] => $this->getOriginalFileName(),
            $keys[8] => $this->getTemporaryFileName(),
            $keys[9] => $this->getReferrer(),
            $keys[10] => $this->getCreatedAt(),
        );

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = RequestPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setSessionId($value);
                break;
            case 2:
                $this->setIpAddress($value);
                break;
            case 3:
                $this->setRequestUrl($value);
                break;
            case 4:
                $this->setPostParameters($value);
                break;
            case 5:
                $this->setGetParameters($value);
                break;
            case 6:
                $this->setMethod($value);
                break;
            case 7:
                $this->setOriginalFileName($value);
                break;
            case 8:
                $this->setTemporaryFileName($value);
                break;
            case 9:
                $this->setReferrer($value);
                break;
            case 10:
                $this->setCreatedAt($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = RequestPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setSessionId($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setIpAddress($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setRequestUrl($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setPostParameters($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setGetParameters($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setMethod($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setOriginalFileName($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setTemporaryFileName($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setReferrer($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setCreatedAt($arr[$keys[10]]);
        }
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(RequestPeer::DATABASE_NAME);

        if ($this->isColumnModified(RequestPeer::ID)) {
            $criteria->add(RequestPeer::ID, $this->id);
        }
        if ($this->isColumnModified(RequestPeer::SESSION_ID)) {
            $criteria->add(RequestPeer::SESSION_ID, $this->session_id);
        }
        if ($this->isColumnModified(RequestPeer::IP_ADDRESS)) {
            $criteria->add(RequestPeer::IP_ADDRESS, $this->ip_address);
        }
        if ($this->isColumnModified(RequestPeer::REQUEST_URL)) {
            $criteria->add(RequestPeer::REQUEST_URL, $this->request_url);
        }
        if ($this->isColumnModified(RequestPeer::POST_PARAMETERS)) {
            $criteria->add(RequestPeer::POST_PARAMETERS, $this->post_parameters);
        }
        if ($this->isColumnModified(RequestPeer::GET_PARAMETERS)) {
            $criteria->add(RequestPeer::GET_PARAMETERS, $this->get_parameters);
        }
        if ($this->isColumnModified(RequestPeer::METHOD)) {
            $criteria->add(RequestPeer::METHOD, $this->method);
        }
        if ($this->isColumnModified(RequestPeer::ORIGINAL_FILE_NAME)) {
            $criteria->add(RequestPeer::ORIGINAL_FILE_NAME, $this->original_file_name);
        }
        if ($this->isColumnModified(RequestPeer::TEMPORARY_FILE_NAME)) {
            $criteria->add(RequestPeer::TEMPORARY_FILE_NAME, $this->temporary_file_name);
        }
        if ($this->isColumnModified(RequestPeer::REFERRER)) {
            $criteria->add(RequestPeer::REFERRER, $this->referrer);
        }
        if ($this->isColumnModified(RequestPeer::CREATED_AT)) {
            $criteria->add(RequestPeer::CREATED_AT, $this->created_at);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(RequestPeer::DATABASE_NAME);
        $criteria->add(RequestPeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of Request (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setSessionId($this->getSessionId());
        $copyObj->setIpAddress($this->getIpAddress());
        $copyObj->setRequestUrl($this->getRequestUrl());
        $copyObj->setPostParameters($this->getPostParameters());
        $copyObj->setGetParameters($this->getGetParameters());
        $copyObj->setMethod($this->getMethod());
        $copyObj->setOriginalFileName($this->getOriginalFileName());
        $copyObj->setTemporaryFileName($this->getTemporaryFileName());
        $copyObj->setReferrer($this->getReferrer());
        $copyObj->setCreatedAt($this->getCreatedAt());
        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(null); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return Request Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return RequestPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new RequestPeer();
        }

        return self::$peer;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->session_id = null;
        $this->ip_address = null;
        $this->request_url = null;
        $this->post_parameters = null;
        $this->get_parameters = null;
        $this->method = null;
        $this->original_file_name = null;
        $this->temporary_file_name = null;
        $this->referrer = null;
        $this->created_at = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volumne/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(RequestPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }
}
