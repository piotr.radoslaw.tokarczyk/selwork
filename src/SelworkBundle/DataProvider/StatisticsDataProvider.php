<?php

namespace SelworkBundle\DataProvider;

use SelworkBundle\Model\RequestQuery;

class StatisticsDataProvider
{
    /**
     * @param \DateTime $date
     *
     * @return int
     */
    public function getRequestCounterByDate(\DateTime $date)
    {
        return RequestQuery::create()->getCountSince($date);
    }
}
