<?php

namespace SelworkBundle\Controller;

use SelworkBundle\Form\UploadType;
use SelworkBundle\Model\File;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * @Route("/user")
     *
     * @return Response
     */
    public function securedAction()
    {
        return $this->render('SelworkBundle:Default:secured.html.twig');
    }

    /**
     * @Route("/user/upload")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function uploadAction(Request $request)
    {
        $file = new File();

        $form = $this->createForm(UploadType::class, $file);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            /** @var UploadedFile $file */
            $uploadedFile = $file->getFile();

            $fileName = md5(uniqid()).'.'.$uploadedFile->guessExtension();

            $uploadedFile->move(
                $this->getParameter('kernel.root_dir').'/../web/uploads',
                $fileName
            );
        }

        return $this->render(
            'SelworkBundle:Default:upload.html.twig', [
                'form' => $form->createView()
            ]
        );
    }
}
