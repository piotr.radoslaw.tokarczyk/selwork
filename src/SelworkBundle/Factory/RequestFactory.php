<?php

namespace SelworkBundle\Factory;

use SelworkBundle\Model\Request;
use Symfony\Component\HttpFoundation\Request as BaseRequest;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class RequestFactory
{
    /**
     * @param BaseRequest $baseRequest
     * @return Request
     */
    public function build(BaseRequest $baseRequest)
    {
        $request = new Request();

        if ($baseRequest->getSession() instanceof SessionInterface) {
            $request->setSessionId($baseRequest->getSession()->getId());
        }

        $request->setIpAddress($baseRequest->getClientIp());
        $request->setRequestUrl($baseRequest->getUri());
        $request->setPostParameters(json_encode($baseRequest->request->all()));
        $request->setGetParameters(json_encode($baseRequest->query->all()));
        $request->setMethod($baseRequest->getMethod());
        $request->setOriginalFileName($baseRequest->files->get('original_name'));
        $request->setTemporaryFileName($baseRequest->files->get('temporary_name'));
        $request->setReferrer($baseRequest->headers->get('referer'));

        return $request;
    }
}
