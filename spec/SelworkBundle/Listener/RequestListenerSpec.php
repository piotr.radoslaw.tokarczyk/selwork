<?php

namespace spec\SelworkBundle\Listener;

use PhpSpec\ObjectBehavior;
use SelworkBundle\Factory\RequestFactory;
use SelworkBundle\Listener\RequestListener;
use SelworkBundle\Model\Request;
use Symfony\Component\HttpFoundation\Request as BaseRequest;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

/**
 * @mixin RequestListener
 */
class RequestListenerSpec extends ObjectBehavior
{
    public function let(AuthorizationCheckerInterface $authorizationChecker, RequestFactory $requestFactory)
    {
        $this->beConstructedWith($authorizationChecker, $requestFactory);
    }

    public function it_should_save_request_on_authenticated_request(
        GetResponseEvent $event,
        BaseRequest $baseRequest,
        AuthorizationCheckerInterface $authorizationChecker,
        Request $request,
        RequestFactory $requestFactory
    ) {
        $event->isMasterRequest()->willReturn(true);
        $event->getRequest()->willReturn($baseRequest);

        $authorizationChecker->isGranted('IS_AUTHENTICATED_FULLY')->willReturn(true);

        $requestFactory->build($baseRequest)->willReturn($request);

        $request->save()->shouldBeCalled();

        $this->onKernelRequest($event);
    }
}
