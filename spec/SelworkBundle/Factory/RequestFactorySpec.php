<?php

namespace spec\SelworkBundle\Factory;

use PhpSpec\ObjectBehavior;
use SelworkBundle\Factory\RequestFactory;
use SelworkBundle\Model\Request;
use spec\SelworkBundle\Stubs\RequestStub;

/**
 * @mixin RequestFactory
 */
class RequestFactorySpec extends ObjectBehavior
{
    public function it_should_build_request_from_base_request()
    {
        $requstStub = new RequestStub();

        $this->build($requstStub)->shouldBeAnInstanceOf(Request::class);
    }
}
