<?php

namespace spec\SelworkBundle\Mail;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use SelworkBundle\Mail\StatisticsMailer;
use SelworkBundle\DataProvider\StatisticsDataProvider;

/**
 * @mixin StatisticsMailer
 */
class StatisticsMailerSpec extends ObjectBehavior
{
    public function let(\Swift_Mailer $mailer, StatisticsDataProvider $dataProvider)
    {
        $this->beConstructedWith($mailer, $dataProvider);
    }

    public function it_should_send_email_with_statistics(
        \Swift_Mime_Message $message,
        $mailer,
        $dataProvider
    ) {
        $dataProvider->getRequestCounterByDate(Argument::cetera())->shouldBeCalledTimes(1)->willReturn(1);

        $mailer->createMessage()->shouldBeCalledTimes(1)->willReturn($message);
        $message->setSubject(Argument::cetera())->shouldBeCalledTimes(1)->willReturn($message);
        $message->setFrom('noreply@sel-work.pl')->shouldBeCalledTimes(1)->willReturn($message);
        $message->setTo('piotr.radoslaw.tokarczyk@gmail.com')->shouldBeCalledTimes(1)->willReturn($message);
        $message->setBody(Argument::cetera())->shouldBeCalledTimes(1)->willReturn($message);

        $mailer->send(Argument::cetera())->shouldBeCalledTimes(1);

        $now = new \DateTime();

        $this->sendForDate($now);
    }
}
